load("augm/Augm_res.rdata")
#worst
##
###
###
###
###
res_comb_plot <- res_comb_worst %>% 
  gather(sim,Q,DMF,Q_new) %>% 
  mutate(sim = factor(sim,levels = c("Q_new","DMF")))
area_plot_plot <- area_plot_worst
title1<- "Required Stream Flow Augmentation for 2012-2013 conditions \nworst case"

#plot without new flows
chart <- ggplot()+
  theme_gray(base_size = 20)+
  geom_ribbon(data=area_plot_plot,aes(Date,ymax = ymax,ymin = ymin,fill = "red" ),alpha = 0.5)+
  facet_wrap(~stream,scales = "free",ncol = 1)+
  geom_line(data=filter(res_comb_plot,sim == "DMF"), aes(Date,Q,col=sim,linetype = sim),size =1.5 )+
  expand_limits(y=0)+
  ggtitle(title1)+
  theme(legend.title=element_blank(),legend.position = "bottom")+
  scale_fill_discrete(labels = c("augmentation flow"))+
  ylab("Daily Mean Flow L/s")+
  scale_linetype_manual ("",
                         values =c("solid"),
                         labels = c("measured flow"))+
  scale_color_manual("",
                     values = c("black"),
                     labels = c("measured flow"))
chart
save_plot("Augm_worst_DMF.png",chart,base_height = 30,base_aspect_ratio = 0.35)


#plot with predicted
chart <- ggplot()+
  theme_gray(base_size = 20)+
  geom_ribbon(data=area_plot_plot,aes(Date,ymax = ymax,ymin = ymin,fill = "red" ),alpha = 0.5)+
  facet_wrap(~stream,scales = "free",ncol = 1)+
  geom_line(data=res_comb_plot, aes(Date,Q,col=sim,linetype = sim),size =1.5 )+
  expand_limits(y=0)+
  ggtitle(title1)+
  theme(legend.title=element_blank(),legend.position = "bottom")+
  scale_fill_discrete(labels = c("augmentation flow"))+
  ylab("Daily Mean Flow L/s")+
  scale_linetype_manual ("",
                         values =c("twodash","solid"),
                         labels = c("measured flow \nplus augmentation flow \nminus combined effect of augmentation pumping",
                                    "measured flow"))+
  scale_color_manual("",
                     values = c("blue","black"),
                     labels = c("measured flow \nplus augmentation flow \nminus combined effect of augmentation pumping",
                                "measured flow"))
 chart
save_plot("Augm_worst.png",chart,base_height = 30,base_aspect_ratio = 0.35)


##recc
##
##
##
res_comb_plot %>%  distinct(sim)
res_comb_plot <- res_comb_recc %>% 
  gather(sim,Q,DMF,Q_new) %>% 
  mutate(sim = factor(sim,levels = c("Q_new","DMF")))
  
area_plot_plot <- area_plot
title1<- "Required Stream Flow Augmentation for 2012-2013 conditions \nrecommended case"
#plot without new flows

chart <- ggplot()+
  theme_gray(base_size = 20)+
  geom_ribbon(data=area_plot_plot,aes(Date,ymax = ymax,ymin = ymin,fill = "red" ),alpha = 0.5)+
  facet_wrap(~stream,scales = "free",ncol = 1)+
  geom_line(data=filter(res_comb_plot,sim == "DMF"), aes(Date,Q,col=sim,linetype = sim),size =1.5 )+
  expand_limits(y=0)+
  ggtitle(title1)+
  theme(legend.title=element_blank(),legend.position = "bottom")+
  scale_fill_discrete(labels = c("augmentation flow"))+
  ylab("Daily Mean Flow L/s")+
  scale_linetype_manual ("",
                         values =c("solid"),
                         labels = c("measured flow"))+
  scale_color_manual("",
                     values = c("black"),
                     labels = c("measured flow"))
chart
save_plot("Augm_recc_DMF.png",chart,base_height = 30,base_aspect_ratio = 0.35)



chart <- ggplot()+
  theme_gray(base_size = 20)+
  geom_ribbon(data=area_plot_plot,aes(Date,ymax = ymax,ymin = ymin,fill = "red" ),alpha = 0.5)+
  facet_wrap(~stream,scales = "free",ncol = 1)+
  geom_line(data=res_comb_plot, aes(Date,Q,col=sim,linetype = sim),size =1.5 )+
  expand_limits(y=0)+
  ggtitle(title1)+
  theme(legend.title=element_blank(),legend.position = "bottom")+
  scale_fill_discrete(labels = c("augmentation flow"))+
  ylab("Daily Mean Flow L/s")+
  scale_linetype_manual ("",
                         values =c("twodash","solid"),
                         labels = c("measured flow \nplus augmentation flow \nminus combined effect of augmentation pumping",
                                    "measured flow"))+
  scale_color_manual("",
                     values = c("blue","black"),
                     labels = c("measured flow \nplus augmentation flow \nminus combined effect of augmentation pumping",
                                "measured flow"))
chart

save_plot("Augm_recc.png",chart,base_height = 30,base_aspect_ratio = 0.35)

