getwd()
library(lubridate)
dat <- read_csv("dmf/Ngaruroro at Fernhill.csv",skip =2)

flow_NGFrn <- dat %>% 
 mutate(Date = parse_date_time(Date,"dmy"),
        DMF = as.numeric(DMF)) 

date_range <- parse_date_time(c("1/6/2012","1/7/2013"),"dmy")

flow_NGFrn_2013 <- flow_NGFrn %>% 
  filter(between(Date,date_range[1],date_range[2])) %>% 
  mutate(data = "recorded flow") %>% 
  rename(flow = DMF,
         date = Date)

flow_diff_NG_fr <- sub_Ng_2012_FH %>% 
  rename(flow = flow_diff) %>% 
  select(date,flow) %>% 
  mutate(data= "abstraction effect")
  
dates_list <- data.frame(date =seq (date_range[1],date_range[2],by="1 days"))
flow_diff_dates <- data.frame(approx(flow_diff_NG_fr$date,flow_diff_NG_fr$flow,dates_list$date)) %>% 
  rename(date = x,
         flow = y) %>% 
  mutate(data = "flow diff")

date_range1 <- parse_date_time(c("1/1/2013","15/4/2013"),"dmy")

flow_diff_NG_fr_nat_wide <- bind_rows(flow_NGFrn_2013,flow_diff_dates) %>% 
  spread(data,flow) %>% 
  rename(`flow recorded` = `recorded flow`) %>% 
  mutate(`flow naturalised` = `flow recorded` +`flow diff`) %>% 
  select (-`flow diff`) %>% 
  filter(between(date,date_range1[1],date_range1[2]))
flow_diff_NG_fr_nat <- flow_diff_NG_fr_nat_wide %>% 
  gather(data,flow,`flow naturalised`, `flow recorded`) %>% 
  mutate(data = factor(data,levels = c("flow recorded","flow naturalised")))



chart <- ggplot()+
  geom_line(data=flow_diff_NG_fr_nat,aes(date,flow,col = data,linetype = data),size =1.5)+
  geom_ribbon(data=flow_diff_NG_fr_nat_wide,
              aes(date,ymin = `flow recorded`,ymax = `flow naturalised` ),
              fill = "blue",alpha = 0.3)+
  theme_gray(base_size = 18)+
  expand_limits(y = 0)+
  ylab("flow L/s")
 
chart
save_plot("Ngaruroro naturalised.png",chart,base_height = 5,base_aspect_ratio = 2)
