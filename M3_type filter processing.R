#scrip for getting effect of different abstraction increases on spring flows
#script was run using hpm035 caclibrated model
#simulation was as M4, but only 2 cycles repeted (M4_ plus10.R)
#how it works:
#  1 loops through several .cbb files
#  2 each iteration runs bud2hyd and processes .cbb file ( .in file is generated on the fly)
#  3 bud2hyd result is processes using custom function
#  4 bud2hyd zones are related to Source catchmetn numbers
#  5 results are combined per give streams. 
#     Streams are defined in source_modflow_zones2.csv file. 
#     this file relates source zone to modflow zones and to spring fed catchments (e.g. Karamu including all upstream catchmetns etc)
#  6 resulsts are then plotted (ther is plot for all springs and for Ngaruroro only)
#  7 script also calculates maximum effect per scenario, stream and plots it
#  7 script will probably won't work automatically, may need some 
#  
library("tidyverse")
library("lubridate")
setwd("C:/PAWEL/MODELLING/HPM/hpm035/hpm035res - Copy")
read_bud2hyd <- function (fname){
  df <- read.table(fname,header = T)
  df$date_time <- paste(df$Date,df$Time)
  df$date_time <- parse_date_time(df$date_time,"dmy HMS")
  nc <- ncol(df)-3
  df <- df %>%
    select(-Date,-Time) %>% 
    gather(zone,"flow",3:nc) 
  df$zone <- as.integer(gsub("Flow_rate_","",df$zone))
  return(df)
}


files1 <- c("_no_irr","_no_PWS","_no_ind") 
files1 <- paste("HPM_M3",files1,sep="")
types <- c("irr","PWS","IND")
i=1
j=1
#incr <- c(-.3,-.2,-.1,-.5,.1,.2,.3,.5,1,0)

f_in <- read_lines("bud2hyd_M3.in")
for (j in 1:length(files1)){
  print(j)
  f_in_1 <- f_in  
  wel_file <- files1[j] #paste("HPM_M3",files1[j],sep="")
  f_in_1[3] <- paste(wel_file,".cbb",sep="")
  f_in_1[13] <- paste(wel_file,".out",sep="")
  f_in_1[16] <- paste(wel_file,".rec",sep="")
  
  f_in_1_file <- paste(wel_file,".in",sep="")
  write_lines(f_in_1,f_in_1_file)
  cmd <- paste("bud2hyd < ",f_in_1_file,sep="")
  shell(cmd)
  
  df1 <- read_bud2hyd(paste(wel_file,".out",sep="")) %>% 
    mutate(sim = files1[j])
  
  
  if(j==1) {res1 <- df1}else{res1 <- bind_rows(res1,df1)}
  
}
distinct(res1,sim)

save(res,file = "flow_by user.rdata")








