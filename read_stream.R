#script to process process bud2hyd
library(tidyverse)
library(lubridate)
wd_org <- getwd()
wd_mod <- "C:/PAWEL/MODELLING/HPM/hpm035/hpm035res - Copy"
setwd(wd_mod)
#function to read
read_bud2hyd <- function (fname){
  df <- read.table(fname,header = T)
  df$date_time <- paste(df$Date,df$Time)
  df$date_time <- parse_date_time(df$date_time,"dmy HMS")
  nc <- ncol(df)-3
  df <- df %>%
    select(-Date,-Time) %>% 
    gather(zone,"flow",3:nc) 
  df$zone <- as.integer(gsub("Flow_rate_","",df$zone))
  return(df)
}
#combine simulation
comb_sim <- function(files_sim){
  sims <- gsub(".out","",files_sim)
  for (i in 1:length(files_sim)){
    df1 <- read_bud2hyd(files_sim[i]) %>% 
      mutate(sim = sims[i])
    if(i==1) {res1 <- df1}else{res1 <- bind_rows(res1,df1)}
  }
  return(res1)
}

#combine zones
comb_str <- function(file1,src_mf){
  #load data by specifying file name
  #file_name <- load(file1) #get new object name and load file
  all_data_comb <- file1 #asign new name
  #rm(list = file_name) #remove loaded object based on name
  #rm(file1,file_name)
  
  
  
  #read in SOURce-Modflow reference table
  # source modflow zones read
  src_mf_zn <- read.csv(src_mf)
  #all zones of interst combined
  src_mf_zn <- src_mf_zn %>% 
    mutate(allzones =1)#add group for all zones
  src_mf_zn_n <- ncol(src_mf_zn)
  #covert to lonf format
  src_mf_zn_long <- src_mf_zn %>% 
    gather (stream_grp,value,6:src_mf_zn_n)  %>% 
    filter (value ==1) %>%
    filter (is.na (modflow.id) == F) %>%
    select(SOURCE_ID,modflow.id,stream_grp)
  
  Q_comb <- all_data_comb %>% 
    full_join(src_mf_zn_long,by=c("zone"="modflow.id")) %>% 
    mutate(stream_grp = ifelse(is.na(stream_grp) == T,"allzones",stream_grp))%>%
    group_by(date_time,Elapsed_Time,stream_grp,sim)%>%
    summarise(flow = sum(flow)) %>%
    ungroup() 
  return (Q_comb)
}

#example:
#define simulations
#files_sim <- c("HPM_M3.out","HPM035_M3_zero.out")
#wd_mod <- "C:/PAWEL/MODELLING/HPM/hpm035/hpm035res - Copy"
#setwd(wd_mod)
#run scrip
#comb_str(comb_sim(files_sim),"source_modflow_zones4.csv")






