library(sf)
library(tmap)
library(OpenStreetMap)
library(tmaptools)
date_sel <- parse_date_time (c("15/2/2030","15/3/2030"),"dmy")

sim_sel <- c("base")
sites_rest %>% distinct(MFSite_MF)

sites_rest <- read_csv("data/M5/Bore_Flow_Management_Site.csv") 
sites_rest1 <- sites_rest %>% 
  rename(bore = BoreNo) %>% 
  filter(is.na(MFSite_MF)==F,
         bore != 0) %>% 
  distinct(bore,.keep_all=T) %>% 
  mutate(bore  = as.character(bore))

res_comb_ban_map<- pumping_dat_sc %>% 
  filter(sim1 %in% sim_sel) %>%
  droplevels() %>% 
  #filter(between(date,date_sel[1],date_sel[2])) %>% 
 # mutate(sim1 = factor(sim1,levels = sim_sel))
 distinct(bore,.keep_all = T) %>% 
  left_join(sites_rest1)

sites2 <- distinct(res_comb_ban_map,MFSite_MF)
sites2 <- sites2$MFSite_MF
dput(sites2)
sites3 <- c(NA, 
            "Raupare_Drain_at_Ormond_Road_1",
            "Raupare_Drain_at_Ormond_Road_2",
            "Ngaruroro_River_at_Fernhill_1",
            "Ngaruroro_River_at_Fernhill_3",
            "Tutaekuri_Waimate_Stm_at_Goods_Bridge", 
            "Tutaekuri_River_at_Puketapu_HBRC_Site_1", 
            "Maraekakaho_Stream_at_Taits_Road"            )
res_comb_ban_map_sf <- res_comb_ban_map %>% 
  st_as_sf(crs = 2193,coords = c("E", "N")) %>% 
  mutate(MFSite_MF = factor(MFSite_MF,levels = sites3))

osm_map <- read_osm(res_comb_ban_map_sf,type ="stamen-terrain")

tm <- qtm(osm_map)+
  tm_shape(res_comb_ban_map_sf)+
  tm_dots(col = "MFSite_MF",
          title="Trigger site",
          #title.size="Metro population",
          size = .5,
          palette = "Set1") +
  tm_scale_bar()+
 tm_compass(position = c("right","top"))
tm 

save_tmap(tm, "restr_sites.png", width=1000, height=1300,dpi = 100)
